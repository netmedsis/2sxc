<img src="https://raw.githubusercontent.com/wiki/2sic/2sxc/assets/logos/2sxc12/2sxc12-500.png" width="200px" align="right">

# 2sxc 12 - CMS & App-Engine for DNN and Oqtane

> you can't use DNN  or Oqtane without 2sxc 😉

2sxc helps web designers and developers prepare great looking, animated and sexy content and applications in DNN (DotNetNuke) and Oqtane. 
In works in .net Framework and .net Core 5.

Visit [2sxc.org](https://2sxc.org/) for more information.

Visit [docs.2sxc.org](https://docs.2sxc.org) for API docs.

Visit [DNN & Razor Tutorials](https://2sxc.org/dnn-tutorials/) for tutorials.

Visit [2sxc Blogs](https://2sxc.org/en/blog) for news, updates, articles.

# Contents of this Repo

1. 2sxc Core - containing the main 2sxc code (not dependant on DNN)
1. 2sxc Dnn - containing everything which connects 2sxc to DNN
1. 2sxc Oqtane - everything which implements 2sxc for Oqtane
1. 2sxc Razor - contains the razor engine and also connects to DNN
1. 2sxc WebApi - contains the webapi - also as part of the DNN infrastructure
1. 2sxc - the root project - containing installation / bundling and dependency injection for runtime

# More Git Repos

This is just one repo of 2sxc - the full software also has another ca. 3 for

1. [EAV-server](https://github.com/2sic/eav-server) the dynamic data layer
1. [EAV-UI](https://github.com/2sic/eav-ui) Angular 12 UI layer for EAV data
1. [2sxc-UI](https://github.com/2sic/2sxc-ui) In-page editing system and JS APIs

