﻿using ToSic.Sxc.Blocks;

namespace ToSic.Sxc.Beta.LightSpeed
{
    public class OutputCacheItem
    {
        public RenderResultWIP Data;

        public bool EnforcePre1025 = true;

    }
}
