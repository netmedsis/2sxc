﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToSic.Sxc.Beta.LightSpeed
{
    internal class OutputCacheConfigState
    {
        public bool Enabled = false;
        public List<int> Apps = new List<int>();

    }
}
