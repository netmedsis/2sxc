﻿namespace ToSic.Sxc.Web.PageService
{
    public partial class PageServiceShared
    {
        public int? HttpStatusCode { get; set; } = null;
        public string HttpStatusMessage { get; set; } = null;

    }
}
