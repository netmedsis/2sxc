﻿using ToSic.Eav.Documentation;

namespace ToSic.Sxc.Data
{
    [PrivateApi]
    internal interface ICanGetNameNotFinal
    {
        dynamic Get(string name);
    }
}
