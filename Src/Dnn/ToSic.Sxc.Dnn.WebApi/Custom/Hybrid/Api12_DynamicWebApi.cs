﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Xml;
using ToSic.Sxc.WebApi;

// ReSharper disable once CheckNamespace
namespace Custom.Hybrid
{
    public abstract partial class Api12 : IDynamicWebApi
    {
        /// <inheritdoc />
        public dynamic File(string dontRelyOnParameterOrder = ToSic.Eav.Parameters.Protector,
            // Important: the second parameter should _not_ be a string, otherwise the signature looks the same as the built-in File(...) method
            bool? download = null,
            string virtualPath = null, // important: this is the virtualPath, but it should not have the same name, to not confuse the compiler with same sounding param names
            string contentType = null,
            string fileDownloadName = null,
            object contents = null // can be stream, string or byte[]
            )
        {
            fileDownloadName = CustomApiHelpers.FileParamsInitialCheck(dontRelyOnParameterOrder, download, virtualPath, fileDownloadName, contents);

            // Try to figure out file mime type as needed
            if (string.IsNullOrWhiteSpace(contentType))
                contentType = MimeMapping.GetMimeMapping(fileDownloadName ?? virtualPath);

            HttpContent httpContent = new ByteArrayContent(Encoding.UTF8.GetBytes(string.Empty));

            // check if this may just be a call to the built in file, which has two strings
            // this can only be possible if only the virtualPath and contentType were set
            if (!string.IsNullOrWhiteSpace(virtualPath))
                httpContent = new StreamContent(new FileStream(HttpContext.Current.Server.MapPath(virtualPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite));

            Encoding encoding = Encoding.UTF8;
            var isValidXml = false;
            switch (contents)
            {
                case XmlDocument xmlDoc:
                    var xmlStream = new MemoryStream();
                    xmlDoc.Save(xmlStream);
                    xmlStream.Position = 0;
                    httpContent = new StreamContent(xmlStream);
                    isValidXml = true;
                    encoding = CustomApiHelpers.GetEncoding(xmlDoc);
                    break;
                case string stringBody:
                    httpContent = new ByteArrayContent(encoding.GetBytes(stringBody));
                    isValidXml = CustomApiHelpers.IsValidXml(stringBody);
                    encoding = CustomApiHelpers.GetEncoding(stringBody);
                    break;
                case Stream streamBody:
                    httpContent = new StreamContent(streamBody);
                    isValidXml = CustomApiHelpers.IsValidXml(streamBody);
                    encoding = CustomApiHelpers.GetEncoding(streamBody);
                    break;
                case byte[] charBody:
                    httpContent = new ByteArrayContent(charBody);
                    isValidXml = CustomApiHelpers.IsValidXml(charBody);
                    encoding = CustomApiHelpers.GetEncoding(charBody);
                    break;
            }
            contentType = CustomApiHelpers.XmlContentTypeFromContent(isValidXml, contentType);

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = httpContent;

            response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType)
            {
                CharSet = encoding.WebName
            };

            // TODO: STV - make sure this is the same in Oqtane
            response.Content.Headers.ContentDisposition = (download == false)
                ? new ContentDispositionHeaderValue("inline")
                : new ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileDownloadName
                };

            return response;
        }
    }
}
